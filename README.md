## SDSS Data Docker for mirror of https://data.sdss.org

cd $DATA_DOCKER_DIR

docker-compose build
docker-compose up -d